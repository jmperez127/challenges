# About the project
The file containing the pattern matcher class is pattern_matcher.rb. <br>
main.rb only runs the specs included in the specs/pattern_matcher_spec.rb file.<br>

The pattern matcher has several ways to use it, here I list a few.

```
PatternMatcher.matches?("aabb", "redredblueblue")
PatternMatcher.match?("aabb", "redredblueblue")
PatternMatcher.new("ababab").match?("redblueredblueredblue")
PatternMatcher.new(pattern: "ababab").match?("redblueredblueredblue")
PatternMatcher.ababbababbababbababb.match?("asdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxx")
pm = PatternMatcher.new
pm.pattern = "aabb"
pm.match?("redredblueblue")
```

All these statements should return true.<br>
Notice that matches? is an alias of match? so they can be used interchangeably.

If you want to see the pattern matcher in use, go to the spec file specs/pattern_matcher_spec.rb

# About the specs framework
For this assignment, I didn't want to use any external gem.<br>
I started working with a small set of assertion functions for testing, but all in all, it began to resemble RSpec more and more, 
so I decided to make a simple implementation of their basic interface.

Success:
```
ruby main.rb
.......................................
```
Failure:
```
ruby main.rb
.F.....................................

'initialize' failed
  it 'should have a flexible interface for pattern matching' in pattern_matcher_spec.rb, line 8
```

The specs framework only consists of a single file with all its classes crammed into it. The file is specs/my_spec.rb.<br> 
Making it didn't take away too much time from the matcher, but instead, it evolved parallel to it. Most of the code needed for the RSpec clone was put together while bored at an airport.


# What was left
Given more time I would have made the matcher work with more than two letters and improved the code for readability and efficiency. 
But I decided to stop and start working on the code review instead.<br>
I enjoyed the time spent in this exercise, 
and I know many things can be improved, but I for now I think it is enough for the problem it intends to solve.


# Instructions 
Given a pattern and a string input - find if the string follows the same pattern and return true or false.

Examples:

Pattern : "abab", input: "redblueredblue" should return true.
Pattern: "aaaa", input: "asdasdasdasd" should return true.
Pattern: "aabb", input: "xyzabcxzyabc" should return false.

Please code your solution using ruby.
