class PatternMatcher
  @@optimized = false

  attr_accessor :pattern, :result, :current_pattern, :prev_input

  def initialize(opts = {optimize_tail_calls: true})
    opts = {pattern: opts, optimize_tail_calls: true} if opts.is_a? String

    @pattern = opts[:pattern]
    optimize_tail_calls if opts[:optimize_tail_calls]
  end

  def match?(input)
    return false if @pattern.size >= input.size - 1
    @result = {}
    @connected_words_count = 0
    words = connect_words_to_pattern_letters(input, pattern)
    input == pattern.split('').map {|p| words[p]}.join('')
  end

  alias_method :matches?, :match?

  private

  # this function will generate a hash like this one
  # {"a"=>"tearing", "b"=>"apart"}
  def connect_words_to_pattern_letters(input, pattern)
    word = get_word(input, pattern.size, input.size / (@pattern.size - @connected_words_count))
    return {} if word == {}

    letter = pattern.slice(0)
    @result[letter] = word if @result[letter].nil?

    new_input = input.gsub(word, '')
    @connected_words_count = input.scan(/(?=#{word})/).count

    new_input.size > 0 ? connect_words_to_pattern_letters(new_input, pattern.gsub(letter, '')) : @result
  end

  def get_word(input, pattern_size, split_pos)
    tmp_input = input.dup
    word = tmp_input[0, split_pos]

    return {} if word.nil?

    tmp_input.gsub!(word, "")
    substitution_count = input.scan(/(?=#{word})/).count

    return word if (len = (pattern_size - substitution_count)) == 0

    split_pos = tmp_input.size / len

    if tmp_input[split_pos, split_pos * 2] == ""
      word
    elsif /#{tmp_input[0, split_pos]}/ !~ tmp_input[split_pos, split_pos * 2]

      return {} if @prev_input == tmp_input
      @prev_input = tmp_input

      get_word(input, pattern_size, split_pos + 1)
    else
      word
    end
  end

  def optimize_tail_calls
    return if @@optimized
    RubyVM::InstructionSequence.compile_option = {
        tailcall_optimization: true,
        trace_instruction: false
    }
    @@optimized = true
  end

  class << self

    def method_missing(name)
      @pattern_matcher ||= PatternMatcher.new
      @pattern_matcher.pattern = name.to_s
      @pattern_matcher
    end

    def match?(pattern, input)
      @pattern_matcher ||= PatternMatcher.new
      @pattern_matcher.pattern = pattern
      @pattern_matcher.match?(input)
    end

    alias_method :matches?, :match?
  end
end