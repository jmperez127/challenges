require_relative "my_spec"
require_relative "../pattern_matcher"

describe "PatternMatcher" do
  describe "initialize" do
    context "When the user is accessing the public api" do

      it "should have a flexible interface for pattern matching" do
        expect(PatternMatcher.matches?("aabb", "redredblueblue")).to eq(true)
        expect(PatternMatcher.match?("aabb", "redredblueblue")).to eq(true)
        expect(PatternMatcher.new("ababab").match?("redblueredblueredblue")).to be_truthy
        expect(PatternMatcher.new(pattern: "ababab").match?("redblueredblueredblue")).to be_truthy
        expect(PatternMatcher.ababbababbababbababb.match?("asdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxx")).to be_truthy
        expect(PatternMatcher.aabbaabb.match?("xyzabcxzyabcxyzabcxzyabc")).to be_falsey
      end
    end

    context "when the user wants to use a PatternMatcher object instance" do
      before(:each) do
        @pm = PatternMatcher.new
      end

      it "should allow to assign a new pattern to the instance and then try to match" do
        @pm.pattern = "ababba"
        expect(@pm.match?("bluegreenbluegreengreenblue")).to be_truthy
      end
    end
  end

  describe "multiple scenarios" do

    context "when the input data and the pattern are a match" do
      it "should return true" do
        expect(PatternMatcher.match?("aabb", "redredblueblue")).to be_truthy
        expect(PatternMatcher.match?("aba", "redbluered")).to be_truthy
        expect(PatternMatcher.match?("ababab", "redblueredblueredblue")).to be_truthy
        expect(PatternMatcher.match?("ababab", "redblueredblueredblue")).to be_truthy
        expect(PatternMatcher.match?("a", "red")).to be_truthy
        expect(PatternMatcher.match?("ab", "redblue")).to be_truthy
        expect(PatternMatcher.match?("aba", "redbluered")).to be_truthy
        expect(PatternMatcher.match?("abba", "redbluebluered")).to be_truthy
        expect(PatternMatcher.match?("abba", "blueredredblue")).to be_truthy
        expect(PatternMatcher.ababbababbababbababb.matches?("asdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxx")).to be_truthy
        expect(PatternMatcher.match?("aaaa", "asdasdasdasd")).to be_truthy
        expect(PatternMatcher.match?("aabb", "redredblueblue")).to be_truthy
        expect(PatternMatcher.match?("aabb", "blueblueredred")).to be_truthy
        expect(PatternMatcher.match?("ababab", "redblueredblueredblue")).to be_truthy
        expect(PatternMatcher.match?("ababab", "blueredblueredbluered")).to be_truthy
        expect(PatternMatcher.match?("abab", "blueredbluered")).to be_truthy
        expect(PatternMatcher.match?("ababab", "tearingaparttearingaparttearingapart")).to be_truthy
        expect(PatternMatcher.match?("abba", "tearingapartaparttearing")).to be_truthy
        expect(PatternMatcher.match?("aaccaa", "bluebluegreengreenblueblue")).to be_truthy
        expect(PatternMatcher.match?("aaaaaa", "asdasdasdasdasdasd")).to be_truthy
        expect(PatternMatcher.match?("abbaaaab", "blueyellowyellowblueblueblueblueyellow")).to be_truthy

      end
    end

    context "when the input data and the pattern don't match" do
      it "should return false" do
        expect(PatternMatcher.match?("aabccb", "xyzabcxzyabcxyzabcxzyabcxyzabcxzyabcxyzabcxzyabc")).to be_falsey
        expect(PatternMatcher.match?("aabbaabb", "xyzabcxzyabcxyzabcxzyabc")).to be_falsey
        expect(PatternMatcher.match?("aacc", "hellohelloworldworlde")).to be_falsey
        expect(PatternMatcher.match?("aacc", "hellohello")).to be_falsey
        expect(PatternMatcher.match?("aaccaa", "redredblueblueredgreen")).to be_falsey
        expect(PatternMatcher.match?("aaccaa", "bluebluegreengreenbluebluer")).to be_falsey
        expect(PatternMatcher.match?("aaccaa", "bluebluegreengreenbluemagenta")).to be_falsey
        expect(PatternMatcher.match?("aba", "blueyellowmagenta")).to be_falsey
        expect(PatternMatcher.match?("ab", "gre")).to be_falsey
        expect(PatternMatcher.match?("abbaaaab", "yellowyellowyellowblueblueblueblueyellow")).to be_falsey
        expect(PatternMatcher.ababbababbababbababb.match?("asdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasdxxxxxxasdxxxasxxxxxxx")).to be_falsey
      end
    end


  end
end
