class String
  def colorize(color_code)
    "\e[#{color_code}m#{self}\e[0m"
  end

  def red
    colorize(31)
  end

  def blue
    colorize(36)
  end

  def green
    colorize(32)
  end
end


module Kernel
  def test_runner
    @@TestRunner ||= MySpec::TestRunner.new
  end
end

module MySpec

  class Test
    attr_accessor :description, :before_hooks, :content, :current_it_block

    def initialize(object, &block)
      @description = object
      @content = block
      @before_hooks = []
    end

    def describe(object, &block)
      test_runner.tests << MySpec::Test.new(object, &block)
    end

    def before(type = :all, &block)
      before_hooks << {type: type, content: block}
      yield if type == :all
    end

    def run
      instance_eval(&@content)
    end

    def it(description, &block)
      before_each_hook = before_hooks.find {|h| h[:type] == :each}

      before_each_hook[:content].call if (before_each_hook)

      @current_it_block = "it '#{description}' in #{block.source_location[0].split('/').last.blue}, line #{block.source_location[1].to_s.blue}"
      yield
    end

    def expect(condition)
      Matcher.new(condition, self)
    end

    def eq(subject)
      subject
    end

    def be_truthy
      true
    end

    def be_falsey
      false
    end

    alias_method :context, :it

  end

  class Matcher
    attr_accessor :condition, :test_object

    def initialize(condition, test_object)
      @condition = condition
      @test_object = test_object
    end

    def not_to(equality)
      eval_condition(@condition != equality)
    end

    def to(equality)
      eval_condition(@condition == equality)
    end

    private

    def eval_condition(condition)
      if condition
        test_runner.output << ".".green
      else
        add_failure_message
      end
    end

    def add_failure_message
      test_runner.output << "F".red
      test_runner.messages[test_object.description] ||= {message: "\n'#{test_object.description}' failed"}
      test_runner.messages[test_object.description][:children] ||= []
      test_runner.messages[test_object.description][:children] << "\s\s#{test_object.current_it_block}"
    end

  end


  class TestRunner
    attr_accessor :tests, :output, :messages

    def initialize
      @tests = []
      @output = ""
      @messages = {}
    end

    def run
      @tests.each {|t| t.run}
      @tests.size
      print_messages
    end

    private

    def print_messages
      puts @output
      @messages.each do |k, v|
        puts v[:message]
        v[:children].each {|c| puts c}
      end
    end

  end

  def describe(object, &block)
    test_runner.tests << MySpec::Test.new(object, &block)
  end

end


include MySpec

