import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Tooltip, OverlayTrigger } from 'react-bootstrap';
import { FormControl } from 'react-bootstrap';
import Autosuggest from 'react-autosuggest';

import './Elements.css';

const inputStyle  = {
  container:                'react-autosuggest__container autosuggest',
  containerOpen:            'react-autosuggest__container--open',
  input:                    'typeahead form-control input-lg input-select__input',
  inputOpen:                'react-autosuggest__input--open',
  inputFocused:             'react-autosuggest__input--focused',
  suggestionsContainer:     'react-autosuggest__suggestions-container dropdown',
  suggestionsContainerOpen: 'react-autosuggest__suggestions-container--open dropdown open',
  suggestionsList:          'react-autosuggest__suggestion-reassign-list dropdown-menu typeahead',
  suggestion:               'react-autosuggest__suggestion',
  suggestionFirst:          'react-autosuggest__suggestion--first',
  suggestionHighlighted:    'react-autosuggest__suggestion--highlighted',
  sectionContainer:         'react-autosuggest__section-container',
  sectionContainerFirst:    'react-autosuggest__section-container--first',
  sectionTitle:             'react-autosuggest__section-title'
};


class InputSelect extends Component {

  constructor(props){
    super(props);
    this.state = {
      selectValue: this.props.selected ? this.props.selected : "",
      inputActive: this.props.inputActive ? true : false,
      suggestionEndpoint: this.props.suggestionEndpoint ? this.props.suggestionEndpoint : null,
      value: ""
    };
  }

  //** Handle state changes  **//

  // Handles the change of the select box
  handleChange = (e) => {
    const target = e.target;
    let other = false;
    // Check if other has been selected
    if(target.value == "other"){ other = true; }

    this.setState({
      selectValue: target.value,
      inputActive: other
    });

    this.props.onChangeListener(target.value);
  }


  // Handling standard input change (Not with Autosuggest)
  handleAutosuggestChange = (event, { newValue }) => {
    this.setState({value: newValue});
    this.props.onChangeListener(newValue);
  }


  // Handles the change of the standard input box
  handleInputChange = (event) => {
    this.setState({value: event.target.value});
    this.props.onChangeListener(event.target.value);
  }


  //** get methods to handle form loading  **//

  getDisabledOption = () => {
    // Method called form select form that will set the disabled option if desired.
    if(this.props.dropdownPlaceholder){
      return (<option value="" disabled> {this.props.dropdownPlaceholder} </option>);
    }
    return null;
  }

  // Method will load the select and it't options
  // this.state.selectValue - Is (Value) the option that is selected
  // this.props.swithToInputText - A (String) containing the message to swith to an input box
  //
  // Examples:
  // this.getSelectForm()
  // => <FormControl> <option> </option>, <option> </option>... </FormControl>
  getSelectForm = () => {
    return (
      <FormControl
        componentClass="select"
        value={this.state.selectValue}
        name="newOwner"
        onChange={this.handleChange}
      >
        {this.getDisabledOption()}
        {this.getOptionsList()}
        <option value="other"> {this.props.swithToInputText} </option>
      </FormControl>
    );
  }

  // Will set the input form type for InputSelect
  // this.props.inputActive - A (boolean) to determine if input element is rendered
  // this.props.suggestionEndpoint - A (String) to set an enpoint for suggestions. CAN BE EMPTY
  //
  // Examples:
  // this.getFormInputType()
  // => <FormControl> </FormControl> <Autosuggest />
  //
  // this.getFormInputType()
  // => <FormControl> <option>, <option> ... </FormControl>
  getFormInputType = () => {
    if(this.state.inputActive) // Check if the input type is selected form the dropdown
    {
      if(this.props.suggestionKey) { // If suggestion key is present, we need to load suggestions

        // explicitly set the constants for autosuggest as values are dynamic (this.props)
        const renderSuggestion = suggestion => (
          <span className="dropdown-item">
            {suggestion[this.props.suggestionKey]}
          </span>
        );
        const getSuggestionValue = suggestion => (
          suggestion[this.props.suggestionKey]
        );
        const { value, suggestions } = this.state;
        const inputProps = {
          type: this.props.suggestionKey,
          placeholder: this.props.inputPlaceholder,
          value,
          onChange: this.handleAutosuggestChange,
          id: "input-select",
          name: this.props.suggestionKey
        };

        return (
          <div>
            {this.getSelectForm()}
            <div className="reassign-input">
              <Autosuggest
                suggestions={this.props.suggestions}
                onSuggestionsFetchRequested={this.props.onSuggestionsFetchRequested}
                onSuggestionsClearRequested={this.props.onSuggestionsClearRequested}
                getSuggestionValue={getSuggestionValue}
                renderSuggestion={renderSuggestion}
                inputProps={inputProps}
                theme={inputStyle}
              />
            </div>
          </div>
        );
      }
      else {
        // We do not want an autosuggest, instead we use a standard input
        return(
          <div>
            {this.getSelectForm()}
            <div className="reassign-input">
              <input className="typeahead form-control input-lg input-select__input"
                     type="text"
                     placeholder={this.props.inputPlaceholder}
                     value={this.state.value}
                     onChange={this.handleInputChange}
              />
            </div>
          </div>
        );
      }
    }
    // Input is not selected just return the list of options
    return this.getSelectForm();
  }

  // Builds the options for the Select element
  // this.props.options - The (Array) of data that will be included into the options
  //
  //
  getOptionsList = () => {
    return (
      this.props.options.map(( option, index ) => {
        return(
          <option
              key={option[this.props.uniqueKey]}
              value={option[this.props.value]}
            >
            {option[this.props.name]}
          </option>
        );
      })
    );
  }

	render() {
		return(
      <div className="input-select">
        { (this.props.options && this.props.options.length > 0) ?
          this.getFormInputType()
          :
          "No options available"
        }
      </div>
		);
	}

}
export default InputSelect;
